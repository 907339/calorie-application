CREATE TABLE dbo.Calories
(
    ID INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
    Calorie INT NOT NULL,
    FoodName_ID INT
    CONSTRAINT FK_Calories_to_FoodName FOREIGN KEY (FoodName_ID) REFERENCES dbo.FoodName (ID)
);
GO