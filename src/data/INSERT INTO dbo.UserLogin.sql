DECLARE @LoginID NVARCHAR(50)
DECLARE @Password NVARCHAR(50)
DECLARE @ReturnValue BIT

SET @LoginID = ''
SET @Password = ''

IF NOT EXISTS ( SELECT 
				'x'
			 FROM dbo.User_Login ul WITH ( NOLOCK )
			 WHERE ul.LoginID = @LoginID )
    BEGIN
	   SET @ReturnValue = 0
    
	   INSERT INTO dbo.User_Login
	   ( LoginID, [Password])
	   VALUES
	   ( @LoginID, @Password);
    END
ELSE
    BEGIN
	   SET @ReturnValue = 1
    END

SELECT @ReturnValue