CREATE TABLE dbo.Meal
(
    ID INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
    MealName NVARCHAR(200) NOT NULL,
    Meal_ID INT NOT NULL, --The ID associated with an entire meal
    FoodName_ID INT NOT NULL
);