CREATE PROCEDURE dbo.Mealpiece_Query
       @Name NVARCHAR(100)
AS
     ( SELECT fn.ID,
              fn.Name,
              fn.DateAdded
       FROM dbo.Foodname AS fn
       WHERE fn.Name = @Name );