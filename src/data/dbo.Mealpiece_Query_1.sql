IF EXISTS( SELECT *
           FROM sys.objects AS o WITH ( NOLOCK )
           WHERE o.type = 'p'
             AND o.name = 'Mealpiece_Query' )
    DROP PROCEDURE dbo.Mealpiece_Query;