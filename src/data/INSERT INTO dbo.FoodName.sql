DECLARE @Name NVARCHAR(100)
DECLARE @DateAdddedd DATETIME
DECLARE @Calorie INT

DECLARE @Calories AS TABLE ( FoodName_ID INT )

SET @Name = ''
SET @Calorie = ''

SET @DateAdddedd = GETUTCDATE()

INSERT INTO dbo.FoodName
    ( 
	   Name, 
	   DateAdded 
    )
OUTPUT inserted.ID 
    INTO @Calories
VALUES 
( @Name, @DateAdddedd );

INSERT INTO dbo.Calories
    ( 
	   Calorie, 
	   FoodName_ID 
    )
SELECT  
    @Calorie , 
    c.FoodName_ID 
FROM    @Calories as c;