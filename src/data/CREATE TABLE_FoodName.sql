--IF EXISTS( SELECT *
--           FROM sys.objects AS o
--           WHERE o.name = 'FoodName' )
--    DROP TABLE dbo.FoodName;

CREATE TABLE dbo.FoodName( ID        INT PRIMARY KEY
                                         IDENTITY( 1, 1 )
                                         NOT NULL,
                           Name      NVARCHAR(100) NOT NULL,
                           DateAdded DATETIME NOT NULL );