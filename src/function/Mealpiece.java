package function;

//import java.sql.SQLException;

import function.Input_FoodItem;
import function.Input_Calories;
import function.Mealpiece_Menu;
import function.SQLInsert_FoodItem;

public class Mealpiece {
	
	private static String Calories;
	private static String Mealname;
	private static boolean menu;
	
	public void MEAL()
	{
		Input_FoodItem piece = new Input_FoodItem();
		Input_Calories calorie = new Input_Calories();
		Mealpiece_Menu MealMenu = new Mealpiece_Menu();
		SQLRead_FoodItem ReadMeal = new SQLRead_FoodItem();
		SQLInsert_FoodItem INSERT = new SQLInsert_FoodItem();
				
	menu = true;
	
	while(menu)
		{	
			MealMenu.MEALMENU();
			try 
			{
				switch(MealMenu.ReturnMenuChoice())
				{
					case 0:
						System.out.println("Exiting menu");
						menu = false;
						break;
					
					case 1:
						System.out.println("Please Enter in ingredient: ");
						Mealname = piece.FoodItem();
						//Mealname = piece.FoodItem();
						
						System.out.println("Please Enter in calories for ingredient: ");
						Calories = calorie.FoodCalories();
						//Calories = calorie.ITEM();
						
						INSERT.InsertData(Mealname, Calories);
		
						//menu = false;
						break;
						
					case 2:
						System.out.println("Enter Meal Item Name");
						
						Mealname = piece.FoodItem();
						//Mealname = piece.ITEM();
						
						ReadMeal.ReturnData(Mealname);
						break;
					
					default:
						System.out.println("default exit");
						menu = false;
						}	
					}	
				catch(Exception e)
			{
				e.printStackTrace();	
			}
		}
	}
	public static String FOODNAME()
	{
		return Mealname;
	}
	public static String CALORIES()
	{
		return Calories;
	}
}