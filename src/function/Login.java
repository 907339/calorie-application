package function;

import function.Login_Verification;
import function.Login_NewUser;
import java.util.Scanner;

public class Login {
	
	private static boolean LoginOn;
	private static int _choice;
	
	static Login_Verification Verification = new Login_Verification();
	static Login_NewUser NewUser = new Login_NewUser();
	
	public static void main(String[] args)
	{
		LoginOn = true;
		
		while(LoginOn)
		{
			System.out.println("Choose one of the following:");
			System.out.println("");
			System.out.println("1: Login");
			System.out.println("");
			System.out.println("2: Create username");
			System.out.println("");
			System.out.println("0: Exit");
			
			Scanner input = new Scanner(System.in);
			_choice = input.nextInt();
			
			try 
			{
				switch(_choice)
				{
				case 0:
					System.out.println("Exiting Application");
					LoginOn = false;
					break;
					
				case 1:
					Verification.Verify();
					if (Verification.verification() == 1)
					{
						LoginOn= false;
					}
					else
					{
						LoginOn = true;
					}
					break;
					
				case 2:
					NewUser.User();
					break;
					
				default:
					System.out.println("Please select one of the menu choices");
					break;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
