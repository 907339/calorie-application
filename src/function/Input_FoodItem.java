package function;
import java.util.Scanner;

public class Input_FoodItem {
	
	//public static final String ITEM = null;
	private static String inputString;
	
	public static String FoodItem()
	{
		try 
		{
			Scanner input = new Scanner(System.in);
			//System.out.println("Enter Meal Item Name");
			inputString = input.nextLine();
			
			if(inputString == null)
			{
				inputString = "";
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception thrown");
			e.printStackTrace();
		}
		
		return inputString;
	}
	//public String ITEM()
	//{
	//	return inputString;
	//}
}