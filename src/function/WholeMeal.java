package function;

import function.Input_FoodItem;
import function.Input_Calories;

import java.util.ArrayList;
import java.util.Scanner;

//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Scanner;

//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class WholeMeal {
	
	//for While loop operation
	private static boolean buildmeal = true;
	
	private static String _foodname;
	private static int _choice;
	private static String _calories;
	private static int _loop;
	private static int _endloop;
	public static int[] array;
	private static ArrayList<String> FullMeal;
	private static String _MealName;
	
	static Meal_Array Meal = new Meal_Array();
	//static Input_FoodItem food = new Input_FoodItem();
	//static Input_Calories calorie = new Input_Calories();
	
	public static void ENTIREMEAL()
	{
		try{
			System.out.println("Enter in items to become a meal");
			//WholeMeal[] entiremeal = new WholeMeal[5];
			
			_loop = 1;
			_endloop = _loop + 1;
			System.out.println(_endloop + " :endloop" + _loop + ": loop");
			
			while(_loop < _endloop)
				try
				{
					_foodname = Input_FoodItem.FoodItem();
					
					if(_foodname == null)
					{
						_foodname = "";
					}
					_calories = Input_Calories.FoodCalories();	
					
					Meal.Work(_foodname, _calories);
					FullMeal = Meal.Return();
					
					System.out.println(FullMeal);
					System.out.println("Want to add another food item?");
					System.out.println("1: Yes");
					System.out.println("0: No");
					
					Scanner input = new Scanner(System.in);
					_choice = input.nextInt();
					
					if(_choice == 1)
					{
						_endloop =_endloop + 1;
						_loop = _loop + 1;
					}
					else
					{
						//_endloop =_endloop + 1;
						_loop = _loop + 1;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			
			Scanner input = new Scanner(System.in);
			System.out.println("What is the name of the Meal?");
			_MealName = input.nextLine();
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

/*
System.out.println(_foodname);
System.out.println(_calories);

System.out.println(entiremeal[0]);
System.out.println(entiremeal[1]);
System.out.println(entiremeal[2]);
System.out.println(entiremeal[3]);
System.out.println(entiremeal[4]);
//if(entiremeal[4] != null)
//	buildmeal = false;
 */

//float, array data type
//float[] theVals = {3,5,10};
//for(int i = 0; i < theVals.length; i++);
//float sum = meal.MEAL(args);
//System.out.println(theVals);
//meal.MEAL(args);

//Potential code to try with array:
//https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html
/*
//Declares Array
String[] anArray;
//Allocates memory for 10 Strings
anArray = new String[10];

anArray[0] = "taco";
anArray[1] = "burrito";
*/