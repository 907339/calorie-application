package function;

import java.sql.Connection;
import java.sql.Statement;

public class SQLInsert_FoodItem {
	
	//private String statement;
	private String queryString;
	private String _Fooditem;
	private String _Calorie;
	
	SQLConnect Connection = new SQLConnect();
	
	public void InsertData(String FoodItem, String Calories)
	{
		try
		{
			_Fooditem = FoodItem;
			_Calorie = Calories;
			
			Connection taco;
			
			taco = Connection.dbConnect();
			Statement statement = taco.createStatement();
			
			queryString = "DECLARE @Name NVARCHAR(100) DECLARE @DateAdddedd DATETIME DECLARE @Calorie INT  DECLARE @Calories AS TABLE ( FoodName_ID INT )  SET @Name = '" + _Fooditem + "' SET @Calorie = " + _Calorie + "  SET @DateAdddedd = GETUTCDATE()  INSERT INTO dbo.FoodName     (  	   Name,  	   DateAdded      ) OUTPUT inserted.ID      INTO @Calories VALUES  ( @Name, @DateAdddedd );  INSERT INTO dbo.Calories     (  	   Calorie,  	   FoodName_ID      ) SELECT       @Calorie ,      c.FoodName_ID  FROM    @Calories as c;";
			
			//ResultSet rs = 
			statement.executeUpdate(queryString);
			
			System.out.println("Added food item: " + _Fooditem + " | Calories: " + _Calorie);
			
			/*
			while (rs.next())
			{
				System.out.println(rs.getString(1));
			}
			*/
			
			statement.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
